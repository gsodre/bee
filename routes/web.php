<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

/**
*Routes to user Auth
*==================================================
*/
Route::get('/login', 'Controller@makelogin')->name('login');
Route::post('/login', 'UsersController@auth')->name('auth');

Route::get('/index', 'Controller@index')->name('index');

/**
*Routes to register user
*==================================================
*/
Route::get('/register', 'UsersController@register')->name('register');

Route::get('/list', 'UsersController@list')->name('list');

Route::resource('user', 'UsersController');
