@extends('templates.master')

@section('view-content')

  <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <img src="{{asset('img/beewt.svg')}}" width="25%">
            </div>

            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">

                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#sobre">a Bee</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contato">Contato</a>
                    </li>
                    <li>
                        <a href="{{ url('login') }}">Entrar <span class="glyphicon glyphicon-triangle-right"></span></a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <img src="{{asset('img/beewt.svg')}}" width="35%" class="img-responsive center-block">
                        <p class="intro-text">Encurtar distâncias, especialista no auxilio de quem precisa de uma "mãozinha"!</p>
                        <a href="{{ route('register') }}" class="btn btn-default btn-lg">Cadastre-se</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- About Section -->
    <section id="sobre" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2> Sobre a Bee </h2>
                <p>Facilitamos encontros felizes entre contratantes e prestadores
                  de serviços.<br>
                  Para isso disponibilizamos uma plataforma completa
                  que auxilia quem precisa contratar e quem presta serviços.
                  E sabe o que é melhor?<br> De maneira descomplicada e eficiente.</p>
            </div>
        </div>
    </section>
    <!-- Contact Section -->
    <section id="contato" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2> Contato </h2>
                <p>Entre em contato conosco através de um de nossos canais!</p>
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="#" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default btn-lg"><i class="fa fa-instagram fa-fw"></i> <span class="network-name">Instagram</span></a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default btn-lg"><i class="fa fa-facebook fa-fw"></i> <span class="network-name">Facebook</span></a>
                    </li>
                </ul>
                <br><br><br><br><br><br><br>
            </div>
        </div>
    </section>

    @endsection
