<!DOCTYPE html>
<html>
  <head>
    <title>Bee -</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Gabriel Sodré" content="">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{asset('usr/bootstrap/css/bootstrap.css')}}">

    <!-- Custom Fonts -->
    <link href="usr/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/bee.css')}}">
  </head>

  @yield('view-content')

    <!-- Footer -->
    <footer></footer>

    <!-- jQuery -->
    <script src="{{asset('usr/jquery/jquery.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('usr/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="{{asset('js/bee.min.js')}}"></script>
  </body>
</html>
