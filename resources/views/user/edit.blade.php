@extends('templates.master')

@section('view-content')

  <body>
    <section class="container content-section-register">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="panel-login">
            <div class="panel-body">
              <div class="section text-center">
                <img src="{{asset('img/beewt.svg')}}" width="50%" class="img-responsive center-block"><br><br>
              </div>

              <h4 class="text-center">Edição de Usuário</h4>

              {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'put', 'role' => 'form']) !!}

              @include('templates.forms.input',['label' => 'Nome', 'input' => 'name', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Seu nome']])
              @include('templates.forms.input',['label' => 'E-mail', 'input' => 'email', 'attributes' => ['class' => 'form-control', 'placeholder' => 'E-mail']])
              @include('templates.forms.input',['label' => 'Telefone', 'input' => 'phone', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Telefone']])
              @include('templates.forms.input',['label' => 'CPF', 'input' => 'cpf', 'attributes' => ['class' => 'form-control', 'placeholder' => 'CPF']])
              @include('templates.forms.input',['label' => 'Data Nasc.', 'input' => 'birth', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Data Nasc.']])
              @include('templates.forms.input',['label' => 'Sexo', 'input' => 'gender', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Sexo']])

              <br>@include('templates.forms.submit',['input' => 'Atualizar', 'attributes' => ['class' => 'btn btn-block btn-default']])
              <a href="{{ route('list') }}" class="btn btn-block btn-default">Cancelar</a>
              {!! Form::close() !!}

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection
