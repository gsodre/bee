@extends('templates.master')

@section('view-content')

  <body>
    <section class="container content-section-register">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="panel-login">
            <div class="panel-body">
              <div class="section text-center">
                <img src="{{asset('img/beewt.svg')}}" width="50%" class="img-responsive center-block"><br><br>
              </div>

              <h4 class="text-center">Cadastro de Usuário</h4>

              <a href="#" class="btn btn-block btn-default">
                <i class="fa fa-2x fa-fw fa-facebook-square"></i> Cadastrar com Facebook</a><br>

                <section>
                  @if(session('success'))
                    <div class="alert alert-success" role="alert">{{ session('success')['messages'] }}</div>
                  @endif
                </section>

                {!! Form::open(['route' => 'user.store', 'method' => 'post', 'role' => 'form']) !!}

                @include('templates.forms.input',['label' => 'Nome', 'input' => 'name', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Seu nome']])
                @include('templates.forms.input',['label' => 'E-mail', 'input' => 'email', 'attributes' => ['class' => 'form-control', 'placeholder' => 'E-mail']])
                @include('templates.forms.input',['label' => 'Telefone', 'input' => 'phone', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Telefone']])
                @include('templates.forms.input',['label' => 'CPF', 'input' => 'cpf', 'attributes' => ['class' => 'form-control', 'placeholder' => 'CPF']])
                @include('templates.forms.input',['label' => 'Data Nasc.', 'input' => 'birth', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Data Nasc.']])
                @include('templates.forms.input',['label' => 'Sexo', 'input' => 'gender', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Sexo']])
                @include('templates.forms.password',['label' => 'Password', 'input' => 'password', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Password']])

                <br>@include('templates.forms.submit',['input' => 'Cadastrar', 'attributes' => ['class' => 'btn btn-block btn-default']])
                <br><a href="{{ route('login') }}" class="btn btn-block btn-default">Login</a>

                {!! Form::close() !!}

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  @endsection
