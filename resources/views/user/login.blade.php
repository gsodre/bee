@extends('templates.master')

@section('view-content')

  <body>
    <section class="container content-section-login">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="panel-login">
            <div class="panel-body">
              <div class="section text-center">
                <img src="{{asset('img/beewt.svg')}}" width="50%" class="img-responsive center-block"><br>
              </div>

              <h4 class="text-center">Login de Usuário</h4>

              {!! Form::open(['route' => 'auth', 'method' => 'post', 'role' => 'form']) !!}

              @include('templates.forms.input',['label' => 'E-mail', 'input' => 'email', 'attributes' => ['class' => 'form-control', 'placeholder' => 'E-mail']])
              @include('templates.forms.password',['label' => 'Password', 'input' => 'password', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Password']])
              <p class="text-right"><small><small><small><a href="#">Esqueceu a senha?</a></small></small></small></p>
            </div>

            @include('templates.forms.submit',['input' => 'Login', 'attributes' => ['class' => 'btn btn-block btn-default']])

            <a href="#" class="btn btn-block btn-default"><i class="fa fa-facebook-square fa-lg"></i> Login com Facebook</a>

            <br><a href="{{ route('register') }}" class="btn btn-block btn-default">Cadastre-se</a>

            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
