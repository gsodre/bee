@extends('templates.master')

@section('view-content')

  <body>
    <section class="container content-section-login">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="section text-center">
            <img src="{{asset('img/beewt.svg')}}" width="50%" class="img-responsive center-block">
            <br>
          </div>
        </div>
      </div>
      <div class="col-md-12">

        <section>
          @if(session('success'))
            <div class="alert alert-success" role="alert">{{ session('success')['messages'] }}</div>
          @endif
        </section>

        <table class="table table-hover table-dark">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nome</th>
              <th scope="col">E-mail</th>
              <th scope="col">Telefone</th>
              <th scope="col">CPF</th>
              <th scope="col">Data Nasc.</th>
              <th scope="col">Sexo</th>
              <th scope="col">Status</th>
              <th scope="col">Permissão</th>
              <th scope="col">Ações</th>
            </tr>
          </thead>
          <tbody>

            @foreach($users as $user)
              <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->cpf }}</td>
                <td>{{ $user->birth }}</td>
                <td>{{ $user->gender }}</td>
                <td>{{ $user->status }}</td>
                <td>{{ $user->permission }}</td>
                <td>
                  <a href="{{ route('user.edit', $user->id) }}" class="btn btn-xs btn-warning"><i class="fa fa-lg fa-fw fa-edit"></i></a>
                  {!! Form::open(['route' => ['user.destroy', $user->id], 'method' => 'DELETE']) !!}
                  <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-lg fa-fw fa-trash-o"></i></button>
                  {!! Form::close() !!}
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>

      </div>
    </section>

  @endsection
