![logo1](/uploads/bb2ca729a562bf52778fafaacd05281b/logo1.png)

# Bee - Serviços Sob Demanda

O sistema intermedia a contratação de serviços, conectando empreendedores individuais que buscam prestar algum tipo de trabalho, a contratantes com a comodidade de alguns cliques.

### Desenvolvedor
Gabriel Sodré

### Supervisor
Prof. Me. Ângelo Gonçalves da Luz <angelogl@gmail.com>

### Requisitos

* <a href="http://www.w3.org/html/">HTML5</a>
* <a href="http://devdocs.io/css/">CSS3</a>
* <a href="https://www.ecma-international.org/ecma-262/6.0/">JavaScript</a>
* <a href="http://php.net/">PHP</a>
* <a href="https://www.mysql.com/">MySQL</a>
* <a href="https://www.apache.org/">Apache</a>
* <a href="https://getbootstrap.com/">BootStrap</a>
* <a href="https://laravel.com/">Laravel</a>
* <a href="https://getcomposer.org/">Composer</a>
* <a href="https://git-scm.com/downloads">Git</a>

### Instalação

1- Abra o Git Bash navegue até o diretório do seu servidor local;

2- Execute os seguintes comandos:;
* git clone https://gitlab.com/gsodre/bee.git
* composer install --no-scripts

3- Copie o arquivo .env.example e o renomeie para .env;
* copy .env.example .env

4- Crie uma nova chave para a aplicação;
* php artisan key:generate

5- Faça a criação do banco de dados;

6- Configure o arquivo .env;

7- Execute as migrations e seeds;

8- Retorne ao Bash e execute o seguinte comando:;
* php artisan serve

9- Acesse: localhost:8000.

## Licença
..
