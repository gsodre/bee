<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollaboratorsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('collaborators', function(Blueprint $table) {
            $table->increments('id');

						//people data
            $table->string('name', 60);
            $table->string('phone', 11)->unique();
            $table->date('birth')->nullable();
            $table->string('cpf', 11)->unique();
            $table->char('gender', 1)->nullable();
						$table->char('marital', 1)->nullable();
						$table->string('schooling', 45)->nullable();
						$table->string('avatar', 255)->nullable();
						$table->string('docFront', 255);
						$table->string('docBack', 255);
						$table->string('criminalRecord', 255)->nullable();
						$table->text('notes')->nullable();

						// auth data
						$table->string('email', 80)->unique();
						$table->string('password', 255)->nullable();

						//permission
						$table->string('status')->default('active');
						$table->string('permission')->default('app.user');

						//sistem data
						$table->rememberToken();
						$table->timestamps();
						$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('collaborators', function(Blueprint $table) {

		});

		Schema::drop('collaborators');
	}

}
