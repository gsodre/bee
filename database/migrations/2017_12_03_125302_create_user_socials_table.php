<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_socials', function (Blueprint $table) {
            $table->increments('id');

            //socials data
            $table->integer('userId')->unsined()->nullable();
            $table->integer('collaboratorId')->unsined()->nullable();
            $table->string('socialNetwork');
            $table->string('socialId');
            $table->string('socialEmail');
            $table->string('socialAvatar');

            //foreing keys
            // $table->foreign('userId')->references('id')->on('users')->nullable();
            // $table->foreign('socialEmail', 80)->references('email')->on('users')->nullable();
            // $table->foreign('collaboratorId')->references('id')->on('collaborators')->nullable();
            // $table->foreign('socialEmail', 80)->references('email')->on('collaborators')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('user_socials', function (Blueprint $table) {
        $table->dropForeign('user_socials_userId_foreing');
        $table->dropForeign('user_socials_collaboratorId_foreing');
        $table->dropForeign('user_socials_socialEmail_foreing');
      });

        Schema::dropIfExists('user_socials');
    }
}
