<?php

use Illuminate\Database\Seeder;
use App\Entities\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create([
        'name'      => 'Tester',
        'phone'     => '53984848484',
        'birth'     => '1987-05-01',
        'cpf'       => '11122233344',
        'gender'    => 'M',
        'email'     => 'test@bee.com',
        'password'  => env('PASSWORD_HASH') ? bcrypt('123456') : '123456',
      ]);
        // $this->call(UsersTableSeeder::class);
    }
}
