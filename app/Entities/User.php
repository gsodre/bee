<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\softDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use softDeletes;
    use Notifiable;


    protected $fillable = [
        'name', 'phone', 'birth', 'cpf', 'gender', 'notes', 'email', 'password', 'status', 'permission',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    *Validation encryption
    *==================================================
    */
    public function setPasswordAttribute($value)
    {
      $this->attributes['password'] = env('PASSWORD_HASH') ? bcrypt($value) : $value;
    }

    // public function getCpfAttribute()
    // {
    //   $cpf = $this->attributes['cpf'];
    //   return substr($cpf, 0, 3). '.' . substr($cpf, 3, 3). '.' . substr($cpf, 6, 3). '-' . substr($cpf, -2);
    // }
    //
    // public function getPhoneAttribute()
    // {
    //   $phone = $this->attributes['phone'];
    //   return '(' . substr($phone, 0, 2). ') ' . substr($phone, 2, 4). '-' . substr($phone, -4);
    // }
    //
    // public function getBirthAttribute()
    // {
    //   $birth = explode('-', $this->attributes['birth']);
    //   $birth = $birth[2] . '/' . $birth[1] . '/' . $birth[0];
    //   return $birth;
    // }
    //
    // public function getGenderAttribute()
    // {
    //   $gender = $this->attributes['gender'];
    //
    //   if ($gender == 'M') {
    //     return 'Masculino';
    //   }
    //   if ($gender == 'F') {
    //     return 'Feminino';
    //   }
    // }
    //
    // public function getStatusAttribute()
    // {
    //   $status = $this->attributes['status'];
    //
    //   if ($status == 'active') {
    //     return 'Ativo';
    //   }
    //   else
    //   {
    //     return 'Inativo';
    //   }
    // }
    //
    // public function getPermissionAttribute()
    // {
    //   $permission = $this->attributes['permission'];
    //
    //   if ($permission == 'app.user') {
    //     return 'Usuário';
    //   }
    // }
}
