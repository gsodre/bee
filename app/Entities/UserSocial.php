<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\softDeletes;

class UserSocial extends Model
{
  use softDeletes;
  use Notifiable;


  protected $fillable = [
   'userId', 'collaboratorId', 'socialNetwork', 'socialId', 'socialEmail', 'socialAvatar',
  ];

  protected $hidden = [];
}
