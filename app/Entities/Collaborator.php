<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\softDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Collaborator extends Model implements Transformable
{
  use TransformableTrait;
  use softDeletes;
  use Notifiable;


  protected $fillable = [
    'name', 'phone', 'birth', 'cpf', 'gender', 'marital', 'schooling',
    'avatar', 'docFront', 'docBack', 'criminalRecord',
    'notes', 'email', 'password', 'status', 'permission',
  ];

  protected $hidden = [
    'password', 'remember_token',
  ];
}
