<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Validators\UserValidator;
use App\Repositories\UserRepository;
use Illuminate\Database\QueryException;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;


class UsersController extends Controller
{

  protected $repository;
  protected $service;

  public function __construct(UserRepository $repository, UserService $service)
  {
    $this->repository = $repository;
    $this->service    = $service;
  }


  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    return view('user.index');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  UserCreateRequest $request
  *
  * @return \Illuminate\Http\Response
  */
  public function store(UserCreateRequest $request)
  {
    $request = $this->service->store($request->all());
    $usuario =  $request['success'] ? $request['data'] : null;

    // dd($request);

    session()->flash('success', [
      'success'   => $request['success'],
      'messages'  => $request['messages']
    ]);

    return view('user.create', [
      'usuario' => $usuario,
    ]);
  }

  /**
  * Display the specified resource.
  *
  * @param  int $id
  *
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $user = $this->repository->find($id);

    if (request()->wantsJson()) {

      return response()->json([
        'data' => $user,
      ]);
    }

    return view('users.show', compact('user'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int $id
  *
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $user = $this->repository->find($id);

    return view('user.edit', [
      'user' => $user
    ]);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  UserUpdateRequest $request
  * @param  string            $id
  *
  * @return Response
  */
  public function update(Request $request, $id)
  {
    $request = $this->service->update($request->all(), $id);
    $usuario =  $request['success'] ? $request['data'] : null;

    // dd($request);

    session()->flash('success', [
      'success'   => $request['success'],
      'messages'  => $request['messages']
    ]);

    // return view('user.list', [
    //   'usuario' => $usuario,
    // ]);
    return redirect()->route('list');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int $id
  *
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $request = $this->service->destroy($id);

    session()->flash('success', [
      'success'   => $request['success'],
      'messages'  => $request['messages']
    ]);

    return redirect()->route('list');
  }


  /**
  *Authentication ontrollers
  *==================================================
  */
  public function auth(Request $request)
  {
    $data = [
      'email'    => $request->get('email'),
      'password' => $request->get('password')
    ];

    try
    {
      if(env('PASSWORD_HASH'))
      {
        \Auth::attempt($data, false);
      }
      else
      {
        $user = $this->repository->findWhere(['email' => $request->get('email')])->first();
        if (!$user)
        throw new Exception("E-mail não cadastrado!");

        if ($user->password != $request->get('password') )
        throw new Exception("Senha inválida!");

        \Auth::login($user);
      }
      return redirect()->route('index');
    }
    catch (Exception $e)
    {
      return $e->getMessage();
    }
  }

  public function register()
  {
    return view('user.create');
  }

  public function list()
  {
    $users = $this->repository->all();

    return view('user.list', [
      'users' => $users

    ]);
  }
}
