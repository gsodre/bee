<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
          //As validações serão feitas aqui e não mais diretamente nos forms
          // 'name'  => 'require',
          // 'phone' => 'require',
          // 'cpf'   => 'require',
          // 'email' => 'require',
        ],
        ValidatorInterface::RULE_UPDATE => [],
   ];
}
